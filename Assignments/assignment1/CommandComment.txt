Comments on Command Line Expressions

ls This command lists the different files and directories in the current directory.

ls -l This command gives more information about the contents of the directory and includes the permissions each ile has, dates created, and the user who created them

ls -a This command shows the hidden files and folders within a directory. The .name files represent the name of the file.

cd This command is sued to change directories. If no parameter is used it takes the user back to the root directory.

cd .. This command moves up one directory from the current one.

rm -r This command removes all directories and files from a given directory if it has anything in it.

rm *.log This command removes all files with the extension .log from a directory

du This command shows the disk usage and the file space that a directory or file takes up.

du -h This puts the output of the normal du command into a more readable format.

mv This command can move files between different directories. It can also rename files such as mv filename newfilename

cp Copies files from on directory to another. It can also copy files into the same directory as a different name

cp *.extension This command copies all files with that certian extension. In order to copy directories you can use the command cp -r

less <filename> This allows a file to be read using a scrolling method.

more <filename> This command allows a file to be read however, instead of sroling a user will read it by moving from page to page.

whoami This shows the userID of the current user.

passwd This allows the user to set or change their password.

wc filename This command gives the number of lines, words and bytes of a given file in that order.

tail -n number of lines  This command gives the last lines typed into the terminal as specified.

diff provides the differences between two different line by line.

