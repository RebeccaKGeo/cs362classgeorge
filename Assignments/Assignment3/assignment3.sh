#!/bin/bash

echo 'Please give the name of file for compile report'
read reportname

for i in $( find . -name '*.tex'); do
     echo $i
     pdflatex $i
done

if [ ! -d "LatexCompileReport" ]; then
     cd
     mkdir LatexCompileReport
     cd -
fi 

wc -w  *.tex *.pdf *.aux *.log > ~/LatexCompileReport/wordcount.txt

echo "
\documentclass[12pt]{article}
\usepackage{geometry,amsmath}
\geometry{hmargin={1in,1in},vmargin={1in,1in}}
\pagestyle{empty}
\usepackage{fancyvrb}
\begin{document}
\begin{center}
{\Large \bf Word Count of the files Compiled}
\end{center}
\VerbatimInput{wordcount.txt}
\end{document} " > ~/LatexCompileReport/$reportname.tex

rm *.aux
rm *.log

echo "***********************************"
echo 'Log of files compiled and created is written in directory ~/LatexCompileReport'
echo "***********************************"
