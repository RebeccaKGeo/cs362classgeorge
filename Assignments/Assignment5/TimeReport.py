from TimeReportFunctions import *
import glob
#This is a script to generate time log reports given a collection of daily time logs

#if len(sys.argv) !=2:
#   print "To Run: python TimeReport.py <textfile>.txt"
#else:
#    print "Using time report from the file", str(sys.argv[1])

TotalTime = []
NameList = []

for i in glob.glob('*.txt'):
    file = open(i,"r")
    print "Reading from file " + i

    TimeLogList = []

    CurrentName = file.readline()
    First, Last = map(str, CurrentName.split())
    EmployeeName = Name(First, Last)
    NameList.append(EmployeeName) 

    for line in file.readlines():
        Day, Hour, Minute = map(str, line.split())
        CurrentTime = TimeReport(Day, Hour, Minute)
        TimeLogList.append(CurrentTime)

    TotalTime.append(addTime(TimeLogList))

DocOutName = "Output.tex"
DocFile = open(DocOutName, "w")

header = ("\\documentclass[12pt]{article} \n"
        + "\\usepackage{geometry} \n"
        + "\\geometry{hmargin={1in,1in},vmargin={1in,1in}} \n"
        + "\\begin{document} \n"
        + "\\thispagestyle{empty} \n\n"
        + "\\begin{center} \n"
        + "{ \\bf Output of Names and Hours Worked} \n"
        + "\\end{center} \n"
        + "Name \qquad \qquad \quad Hour \qquad\qquad Minute \n\n"
        + "\\begin{flushleft}")
DocFile.write(header)

for j in range(len(NameList)):

    body  = (NameList[j].First + " " + NameList[j].Last + " \quad \qquad" 
          + str(TotalTime[j]) + "\n\n")
    DocFile.write(body)

footer = "\\end{flushleft}\n\\end{document}\n"
DocFile.write(footer)
DocFile.close

print "A report named Output.tex contains the names and total hours worked" 
