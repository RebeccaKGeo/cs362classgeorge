import sys
import os
import math
from string import whitespace

class Name:
    def __init__ (self, First, Last):
        self.First = First
        self.Last  = Last

    def dumpName(self):
        print "Name: " + self.First + " " + self.Last

class TimeReport:
    def __init__(self, Day, Hour, Minute):
        self.Day    = Day
        self.Hour   = Hour
        self.Minute = Minute

    def dumpTimeReport(self):
        print self.Day + " " + self.Hour + " " + self.Minute
 

def addTime(TimeLog):
    sumMin = 0
    sumHour = 0
    for i in range (len(TimeLog)): 
        str_hour = ''.join(h for h in TimeLog[i].Hour if h.isdigit())
        int_hour = int(str_hour)

        str_min  = ''.join(m for m in TimeLog[i].Minute if m.isdigit())
        int_min  = int(str_min)

        sumMin = sumMin + int_min
        sumHour = sumHour + int_hour

    tempHour = sumMin / 60
    tempMin = sumMin % 60

    sumHour = sumHour + tempHour
    return str(sumHour) + "\qquad \qquad \quad" + str(tempMin)




