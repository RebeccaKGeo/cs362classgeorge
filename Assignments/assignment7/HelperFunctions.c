/*  This is a bunch of stuff to use in a pixel mapping for the general shape of a pumpkin*/

#include <math.h> //Allows the use of pwr functions pow(x,y) -----> x^y

int InCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

    int InOrOut = 0; //Int. flag for if the pixel is in the circle or not

    int CenterX = totalCols / 2;
    int CenterY = totalRows / 2;
    int distance = 0; //Distance from the center to pixel

    distance = pow((pow(CenterX - pixCol,2)) + (pow(CenterY - pixRow,2)), 0.5);

    if (distance < radius){
        InOrOut = 1;
    } 

    return InOrOut;
}

int greenTop(int totalRows, int totalCols, int radius, int pixRow, int pixCol){
     int InOrOut = 0; //Integer flag for being in the stem
                      // 1 == true, 0 == false
     int CenterX = totalCols / 2;

     if(pixCol < CenterX + 20 && pixCol > CenterX - 20){
       if(pixRow > 0 && pixRow < totalRows / 8){
         InOrOut = 1; 
       }
     }

     return InOrOut;
}
