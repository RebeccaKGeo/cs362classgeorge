/* The header file */
/* Function to see if the value of the pixel is in the circle or not */

int InCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int SmallCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int LeftEye(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int greenTop(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int RightEye(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int mouth(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
