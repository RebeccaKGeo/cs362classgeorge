/*  This is a bunch of stuff to use in a pixel mapping for the facial features of a pumpkin*/

#include <math.h> //Allows the use of pwr functions pow(x,y) -----> x^y

int SmallCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

    int InOrOut = 0; //Integer flag set if the pixel is in the middle of the circle

    int CenterX = totalCols / 2;
    int CenterY = totalRows / 2;
    int distance = 0;

    distance = pow((pow(CenterX - pixCol, 2)) + (pow(CenterY - pixRow,2)), 0.5);

    if (distance < radius / 6){
       InOrOut = 1;
    }

    return InOrOut;
}

int LeftEye(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

    int InOrOut = 0;

    int CenterX = totalCols / 2;
    int CenterY = totalRows / 2; 
    
    int distance = 0;
    distance = pow((pow(CenterX - pixCol, 2)) + (pow(CenterY - pixRow,2)), 0.5);

    if (distance < radius){
       if(pixRow > ((totalRows / 8) * 2.5) && pixRow < CenterY){
         if(pixCol > (totalCols / 16) * 3 && pixCol < (totalCols / 16)* 6){
            InOrOut = 1;
         }
       }
    }

    return InOrOut;
}

int RightEye(int totalRows, int totalCols, int radius, int pixRow, int pixCol){
    int InOrOut = 0;
    int CenterX = totalCols / 2;
    int CenterY = totalRows / 2;

    int distance = 0;
    distance = pow((pow(CenterX - pixCol, 2)) + (pow(CenterY - pixRow,2)), 0.5);

    if (distance < radius){
       if(pixRow > ((totalRows / 8) * 2.5) && pixRow < CenterY){
         if(pixCol > CenterX+(totalCols/16) * 2 && pixCol < CenterX+(totalCols / 16) * 5){
            InOrOut = 1;
         }
       }
    }

    return InOrOut;
}

int mouth( int totalRows, int totalCols, int radius, int pixRow, int pixCol){
    int InOrOut = 0;
    int CenterX = totalCols / 2;
    int CenterY = totalRows / 2;
    int distance = 0;

    distance = pow((pow(CenterX - pixCol, 2)) + (pow(CenterY - pixRow,2)), 0.5);

    if (distance < radius){
       if(pixRow > CenterY + (totalRows / 16)* 2  && pixRow < CenterY + (totalRows / 16) * 4){
         if(pixCol > (totalCols / 16) * 4 && pixCol < (totalCols / 16) * 13){
            InOrOut = 1;
         }
       }
    }

    return InOrOut;
}


